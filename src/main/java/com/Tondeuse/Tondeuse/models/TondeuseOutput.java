package com.Tondeuse.Tondeuse.models;

import lombok.Data;

@Data
public class TondeuseOutput {
    private int finalX;
    private int finalY;
    private Direction finalDirection;

    public TondeuseOutput(int finalX, int finalY, Direction finalDirection) {
        this.finalX = finalX;
        this.finalY = finalY;
        this.finalDirection = finalDirection;
    }
}
