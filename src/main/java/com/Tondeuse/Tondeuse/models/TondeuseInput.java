package com.Tondeuse.Tondeuse.models;

import lombok.Data;

@Data
public class TondeuseInput {
    private int width;
    private int height;
    private int x;
    private int y;
    private Direction direction;
    private String instructions;
}
