package com.Tondeuse.Tondeuse.models;

import lombok.Getter;

public class Tondeuse {
    @Getter
    private int x;
    @Getter
    private int y;
    @Getter
    private Direction direction;
    private Jardin jardin;

    public Tondeuse(int x, int y, Direction direction, Jardin jardin) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.jardin = jardin;
    }

    public void turnLeft() {
        switch(this.direction){
            case EAST -> direction=Direction.NORTH;
            case WEST -> direction=Direction.SOUTH;
            case NORTH -> direction=Direction.WEST;
            case SOUTH -> direction=Direction.EAST;
        }
    }

    public void turnRight() {
        switch(this.direction){
            case EAST -> direction=Direction.SOUTH;
            case WEST -> direction=Direction.NORTH;
            case NORTH -> direction=Direction.EAST;
            case SOUTH -> direction=Direction.WEST;
        }
    }

    public void move() {
        if (direction==Direction.NORTH && y < jardin.getHeight()) {
            y++;
        } else if (direction==Direction.SOUTH && y > 0) {
            y--;
        } else if (direction==Direction.WEST && x < jardin.getWidth()) {
            x--;
        } else if (direction==Direction.EAST && x > 0) {
            x++;
        }
    }

}
