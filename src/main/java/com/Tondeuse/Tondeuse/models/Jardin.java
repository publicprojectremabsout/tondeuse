package com.Tondeuse.Tondeuse.models;

public class Jardin {
    private int width;
    private int height;

    public Jardin(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
