package com.Tondeuse.Tondeuse.config;

import com.Tondeuse.Tondeuse.batchUtils.TondeuseWriter;
import com.Tondeuse.Tondeuse.models.TondeuseInput;
import com.Tondeuse.Tondeuse.models.TondeuseOutput;
import com.Tondeuse.Tondeuse.processor.TondeuseProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class TondeuseConfiguration {
    @Bean
    public Job tondeuseJob(JobRepository jobRepository, Step tondeuseStep1){
        return new JobBuilder("tondeuseJob", jobRepository)
                .start(tondeuseStep1)
                .build();
    }

    @Bean
    public Step tondeuseStep1(PlatformTransactionManager transactionManager,
                              JobRepository jobRepository,
                              ItemProcessor<TondeuseInput,TondeuseOutput> tondeuseProcessor,
                              ItemReader<TondeuseInput> inputReader,
                              ItemWriter<TondeuseOutput> tondeuseWriter
                             ){
        return new StepBuilder("tondeuseStep1",jobRepository)
                .<TondeuseInput,TondeuseOutput>chunk(1,transactionManager)
                .reader(inputReader)
                .processor(tondeuseProcessor)
                .writer(tondeuseWriter)
                .build();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<TondeuseInput> inputReader(@Value("#{jobParameters[inputFile]}") String resource) {
        return new FlatFileItemReaderBuilder<TondeuseInput>()
                .name("inputFileReader")
                .resource(new FileSystemResource(resource))
                .lineTokenizer(new DelimitedLineTokenizer() {{
                    setDelimiter(" ");
                    setNames("width", "height");
                    setIncludedFields(0, 1);
                }})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>(){{
                    setTargetType(TondeuseInput.class);
                }})
                .build();
    }
    @Bean
    @StepScope
    public ItemWriter<TondeuseOutput> tondeuseWriter() {
        return new TondeuseWriter();
    }
    @Bean
    @StepScope
    public ItemProcessor<TondeuseInput, TondeuseOutput> processor(){
        return new TondeuseProcessor();
    }

}
