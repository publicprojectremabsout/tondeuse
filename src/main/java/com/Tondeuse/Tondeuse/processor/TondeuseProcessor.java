package com.Tondeuse.Tondeuse.processor;


import com.Tondeuse.Tondeuse.models.Jardin;
import com.Tondeuse.Tondeuse.models.Tondeuse;
import com.Tondeuse.Tondeuse.models.TondeuseInput;
import com.Tondeuse.Tondeuse.models.TondeuseOutput;
import org.springframework.batch.item.ItemProcessor;

public class TondeuseProcessor implements ItemProcessor<TondeuseInput, TondeuseOutput> {
    @Override
    public TondeuseOutput process(TondeuseInput input) throws Exception {
        Jardin jardin = new Jardin(input.getWidth(), input.getHeight());
        Tondeuse tondeuse = new Tondeuse(input.getX(), input.getY(), input.getDirection(), jardin);
        if (input.getInstructions() != null){
            for (char instruction : input.getInstructions().toCharArray()) {
                switch (instruction) {
                    case 'G':
                        tondeuse.turnLeft();
                        break;
                    case 'D':
                        tondeuse.turnRight();
                        break;
                    case 'A':
                        tondeuse.move();
                        break;
                    default:
                }
            }
    }
        return new TondeuseOutput(tondeuse.getX(), tondeuse.getY(), tondeuse.getDirection());
    }
}
