package com.Tondeuse.Tondeuse.batchUtils;

import com.Tondeuse.Tondeuse.models.TondeuseOutput;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ItemWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TondeuseWriter implements ItemWriter<TondeuseOutput> {

    @Override
    public void write(Chunk<? extends TondeuseOutput> chunk) throws Exception {
            for (TondeuseOutput item : chunk.getItems()) {
                System.out.println(item.getFinalX() + " " + item.getFinalY() + " " + item.getFinalDirection());

            }
    }
}
